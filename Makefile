.PHONY: all
all:
	env GOOS=windows GOARCH=amd64 go build -o bin/ngd_windows-amd64.exe src
	env GOOS=linux GOARCH=amd64 go build -o bin/ngd_linux-amd64 src
	env GOOS=darwin GOARCH=amd64 go build -o bin/ngd_darwin-amd64 src

local:
	cp docs/ngd.pdf README.md public/
	go run local.go