# Normalized google distance

[Paper here](ngd.pdf)

This program will query a given search engine for every pairwise-combinations of given set of (English) words, and return the number of hits reported by each results page.  The results are printed to standard out.

## Installation

Download the latest version for your architecture from [tags](https://gitlab.com/taqui/ngd/tags).

Rename it `ngd` or `ngd.exe` on windows, and place it someplace in your `PATH` environment variable.

## Usage

#### Flags

##### `-engine`

Search engine to use: `bing` or `google` (default).

##### `-site`

Optionally restrict search to a given site.  Default is to search all of the internet.

##### `-terms`

Location of a text file containing words to search for, each given on a new line.  For example,

```
computation
external
domesticated
disclosure
pavement
blinks
gimmick
dynasty
conversation
reason
```

##### `-out`

Output type: `yaml` (prints any errors) or `csv` (default).

#### Example

```bash
ngd -terms words.txt

ngd -terms words.txt -engine bing -site wikipedia.com -out yaml
```