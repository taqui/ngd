package query

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

// Result holds the input and outputs of a query
type Result struct {
	Engine string        `yaml:"engine"`
	Site   string        `yaml:"site"`
	Hits   chan HitCount `yaml:"-"`
	Counts []HitCount    `yaml:"counts"`
	Err    error         `yaml:"error"`
}

// HitCount describes the terms and associated search result size
type HitCount struct {
	Terms []string `yaml:"terms"`
	Count int64    `yaml:"count"`
	Err   string   `yaml:"error"`
}

// Engine holds data required to scrape a particular search engine
type Engine struct {
	// BaseURL is the search query url without the parameters
	BaseURL string
	// SiteURL is the site-specific component within the search url
	SiteURL string
	// Regexp is the regular expression used to extract the hit count
	Regexp *regexp.Regexp
}

// Engines availabe to query
var Engines = map[string]Engine{
	"google": Engine{
		BaseURL: "https://www.google.com/search?lr=lang_en&q=",
		SiteURL: "&site=",
		Regexp:  regexp.MustCompile(`>About (\d{1,3}[,?\d{3}]*) results<`),
	},
	"bing": Engine{
		BaseURL: "https://www.bing.com/search?q=language%3Aen%20",
		SiteURL: "%20domain%3A",
		Regexp:  regexp.MustCompile(`>(\d{1,3}[,?\d{3}]*) results<`),
	},
	"duckduckgo": Engine{
		BaseURL: "https://wwww.duckduckgo.com/?ia=web&q=",
		Regexp:  regexp.MustCompile(``),
	},
}

// Search and return the number of results per item
func Search(engine string, terms [][]string, site string) (res *Result) {
	res = &Result{
		Engine: engine,
		Site:   site,
		Hits:   make(chan HitCount),
		Counts: []HitCount{},
	}
	for _, cmb := range terms {
		go func(cmb []string) {
			res.Hits <- res.search(cmb)
		}(cmb)
	}
	// wait for goroutines to finish
	for _ = range terms {
		res.Counts = append(res.Counts, <-res.Hits)
	}
	return
}

func (res *Result) search(cmb []string) (hit HitCount) {
	hit = HitCount{Terms: cmb}
	engine, ok := Engines[res.Engine]
	if !ok {
		panic(fmt.Errorf("engine %s is not defined", res.Engine))
	}
	resp, err := http.Get(engine.URL(cmb, res.Site))
	if err != nil {
		hit.Err = err.Error()
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		hit.Err = err.Error()
		return
	}
	match := engine.Regexp.FindAllStringSubmatch(string(body), -1)
	if len(match) < 1 {
		hit.Err = fmt.Sprintf("unmatched result: %s\n%s\n", match, string(body))
		return
	}
	if len(match[0]) < 2 {
		hit.Err = fmt.Sprintf("empty match: %s\n", match[0])
		return
	}
	submatch := strings.Replace(match[0][1], ",", "", -1)
	count, err := strconv.ParseInt(submatch, 10, 64)
	if err != nil {
		hit.Err = err.Error()
	}
	hit.Count = count
	return
}

// URL generates the search endpoint from TemplateURL and parameters
func (e Engine) URL(terms []string, site string) (url string) {
	url = e.BaseURL + combine(terms)
	if len(site) > 0 {
		url += e.SiteURL + site
	}
	return
}

func combine(terms []string) (cmb string) {
	cmb = terms[0]
	for _, term := range terms[1:] {
		cmb += "+" + term
	}
	return
}
