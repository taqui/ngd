package query

import "testing"

type URLTest struct {
	engine string
	Terms  []string
	Site   string
	URL    string
}

var testTerms = []string{"golang", "python"}

var URLTests = map[string]URLTest{
	"google-nosite": {
		engine: "google",
		Terms:  testTerms,
		URL:    "https://www.google.com/search?lr=lang_en&q=golang+python",
	},
	"google-wikipedia": {
		engine: "google",
		Terms:  testTerms,
		Site:   "www.wikipedia.com",
		URL:    "https://www.google.com/search?lr=lang_en&q=golang+python&site=www.wikipedia.com",
	},
	"bing-nosite": {
		engine: "bing",
		Terms:  testTerms,
		URL:    "https://www.bing.com/search?q=language%3Aen%20golang+python",
	},
	"bing-wikipedia": {
		engine: "bing",
		Terms:  testTerms,
		Site:   "www.wikipedia.com",
		URL:    "https://www.bing.com/search?q=language%3Aen%20golang+python%20domain%3Awww.wikipedia.com",
	},
}

func TestURL(t *testing.T) {
	for descrip, tc := range URLTests {
		engine := Engines[tc.engine]
		url := engine.URL(tc.Terms, tc.Site)
		if url != tc.URL {
			t.Errorf("%s URL not matching\n\twant:\t%s\n\tgot:\t%s\n", descrip, tc.URL, url)
		}
	}
}

func TestQuery(t *testing.T) {
	result := Search("google", [][]string{{"golang", "python"}}, "")
	if result.Err != nil {
		t.Error(result.Err)
	}
	if len(result.Counts) == 0 {
		t.Error("no result")
	}
	if len(result.Counts[0].Err) > 0 {
		t.Error(result.Counts[0].Err)
	}
	if result.Counts[0].Count == 0 {
		t.Error("no hits")
	}
}
