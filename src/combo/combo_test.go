package combo

import (
	"fmt"
	"testing"
)

type comboTest struct {
	terms []string
	count int
}

var comboTests = map[string]comboTest{
	"empty": {
		terms: []string{},
		count: 0,
	},
	"single": {
		terms: []string{"1"},
		count: 0,
	},
	"simple": {
		terms: []string{"1", "2"},
		count: 1,
	},
	"lots": {
		terms: []string{"1", "2", "3", "4", "5"},
		count: 10,
	},
}

func TestCombo(t *testing.T) {
	for descrip, tc := range comboTests {
		combos := Create(tc.terms)
		if len(combos) != tc.count {
			t.Errorf("%s expected: %d\tgot: %d\n", descrip, tc.count, len(combos))
			fmt.Printf("%+v\n", combos)
		}
	}
}
