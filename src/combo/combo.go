package combo

import (
	"io/ioutil"
	"strings"
)

// Create will convert a list of words into a pairwise list
// of every (unordered) combination
func Create(terms []string) (combos [][]string) {
	for i, term := range terms {
		if i == len(terms)-1 {
			return
		}
		combos = append(combos, join(term, terms[i+1:])...)
	}
	return
}

func join(term string, terms []string) (combos [][]string) {
	for _, term2 := range terms {
		combos = append(combos, []string{term, term2})
	}
	return
}

// FromFile reads words from a file and returns all combinations
func FromFile(filename string) (combos [][]string, err error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return
	}
	content := strings.Replace(string(b), "\r", "", -1)
	allTerms := strings.Split(content, "\n")
	combos = Create(allTerms)
	return
}
