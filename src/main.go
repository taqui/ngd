package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"strconv"
	"time"

	"gitlab.com/taqui/ngd/package/combo"
	"gitlab.com/taqui/ngd/package/query"
	yaml "gopkg.in/yaml.v2"
)

var (
	engine = flag.String("engine", "google", "search engine used {google (default), bing, duckduckgo}")
	site   = flag.String("site", "", "optionally restrict search specified site only")
	terms  = flag.String("terms", "", "file containing search terms")
	output = flag.String("out", "csv", "output format {yaml, csv (default)}")
)

func init() {
	flag.Parse()
}

func main() {
	start := time.Now()
	combos, err := combo.FromFile(*terms)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Searching %s... ", *site)
	results := query.Search(*engine, combos, *site)
	fmt.Println(time.Now().Sub(start))
	switch *output {
	case "yaml":
		printYAML(*results)
		return
	case "csv":
	default:
		fmt.Println("unrecognized output format")
	}
	printCSV(*results)
}

func printYAML(results query.Result) {
	out, err := yaml.Marshal(results)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(out))
}

func printCSV(results query.Result) {
	out := csv.NewWriter(os.Stdout)
	for _, hit := range results.Counts {
		out.Write(append(hit.Terms, strconv.Itoa(int(hit.Count))))
	}
	out.Flush()
}
