# Changelog

## 2017-09-09
__0.1.1__

* add Bing search engine
* csv default output type
* english language results only

## 2017-09-04
__0.1.0__

* Initial version
* scrape result count concurrently